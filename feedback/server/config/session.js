const { sessionKey } = require("../configuration");
const session = require("client-sessions");

module.exports = session({
  cookieName: "session",
  secret: sessionKey,
  duration: 7200000,
  activeDuration: 300000,
  httpOnly: true,
  ephemeral: true
});
