const express = require("express");
const router = express.Router();
const { student, professor, auth, reset } = require("../controllers");

//reset
router.get("/reset", reset.reset);

//auth
router.post("/login", auth.login);
router.get("/logout", auth.logout);

//professor
router.post("/professors", professor.rgister);
router.post("/activities", auth.middleware.checkLogin, professor.addActivity);
router.get("/activities", auth.middleware.checkLogin, professor.getActvities);
router.get(
  "/activities/:activityId",
  auth.middleware.checkLogin,
  professor.getOneActivity
);

//student
router.post("/student", student.getAccess);
router.post("/feedback", auth.middleware.checkLogin, student.giveFeedback);

module.exports = router;
