const { User, Activity, Feedback } = require("../models");

const controller = {
  rgister: async (req, res) => {
    try {
      const { firstName, lastName, email, password } = req.body;

      const errors = [];

      if (!firstName) {
        errors.push("First name is empty");
      }
      if (!lastName) {
        errors.push("Last name is empty");
      }
      if (!email) {
        errors.push("Email is empty");
      } else if (
        !email.match(
          /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
        )
      ) {
        errors.push("Email is not valid");
      } else if (await User.findOne({ where: { email }, raw: true })) {
        errors.push("Email already used");
      }
      if (!password) {
        errors.push("Password is empty");
      }

      if (errors.length === 0) {
        await User.create({
          firstName,
          lastName,
          email,
          password,
          type: 1,
          token: Math.random().toString(36)
        });
        res.status(201).send({
          message: `Professor ${firstName} ${lastName} was sucessfull created`
        });
      } else {
        res.status(400).send({ errors });
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  },
  addActivity: async (req, res) => {
    try {
      const { duration, description } = req.body;

      const errors = [];

      if (!description) {
        errors.push("Description is empty");
      }
      if (!duration) {
        errors.push("Duration is empty");
      } else if (!duration.match(/^[0-9]*$/)) {
        errors.push("Duration is not valid");
      }

      if (errors.length === 0) {
        const activity = await Activity.create({
          description,
          accessCode: Math.random()
            .toString(36)
            .slice(6),
          duration,
          userId: req.session.id,
          status: 1
        });
        res.status(201).send({
          message: `Activity ${description} was sucessfull created`,
          accessCode: activity.accessCode
        });
      } else {
        res.status(400).send({ errors });
      }
      //
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  },
  getActvities: async (req, res) => {
    try {
      const activities = await Activity.findAll({
        attributes: [
          "id",
          "accessCode",
          "duration",
          "status",
          "description",
          "createdAt"
        ],
        where: {
          userId: req.session.id
        }
      });
      const formattedActivities = await Promise.all(
        await activities.map(async item => {
          const itemTime = new Date(item.createdAt);

          if (itemTime.getTime() + item.duration * 60 * 1000 < Date.now()) {
            await item.update({ ...item, status: 0 });
          }

          const feedbacks = await Feedback.findAll({
            attributes: ["emoticon", "createdAt"],
            where: {
              activityId: item.id
            }
          });

          return { ...item.get({ plain: true }), feedbacks };
        })
      );

      res.status(200).send(formattedActivities);
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  },
  getOneActivity: async (req, res) => {
    try {
      const { activityId } = req.params;
      const activity = await Activity.findOne({
        attributes: [
          "id",
          "accessCode",
          "duration",
          "status",
          "description",
          "createdAt"
        ],
        where: {
          id: activityId
        }
      });
      const itemTime = new Date(activity.createdAt);

      if (itemTime.getTime() + activity.duration * 60 * 1000 < Date.now()) {
        await activity.update({ ...activity, status: 0 });
      }

      const feedbacks = await Feedback.findAll({
        attributes: ["emoticon", "createdAt"],
        where: {
          activityId
        }
      });
      res.status(200).send({ ...activity.get({ plain: true }), feedbacks });
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  }
};

module.exports = controller;
