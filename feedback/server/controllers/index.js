const controllers = {
  reset: require("./reset"),
  auth: require("./auth"),
  professor: require("./professor"),
  student: require("./student")
};

module.exports = controllers;
