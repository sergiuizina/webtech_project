const { User, Activity, Feedback } = require("../models");

const controller = {
  getAccess: async (req, res) => {
    try {
      const { accessCode, firstName, lastName, email } = req.body;

      const activity = await Activity.findOne({
        attributes: ["id", "description", "duration", "createdAt", "userId"],
        where: { accessCode, status: 1 }
      });

      if (!activity) {
        res.status(404).send({
          message: `Activity not found`
        });
      } else {
        const itemTime = new Date(activity.createdAt);

        if (itemTime.getTime() + activity.duration * 60 * 1000 < Date.now()) {
          await activity.update({ ...activity, status: 0 });
        }
        let user;
        const errors = [];
        if (email) {
          user = await User.findOne({ where: { email }, raw: true });
        }
        if (!user) {
          if (!firstName) {
            errors.push("First name is empty");
          }
          if (!lastName) {
            errors.push("Last name is empty");
          }
          if (!email) {
            errors.push("Email is empty");
          } else if (
            !email.match(
              /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
            )
          ) {
            errors.push("Email is not valid");
          } else if (await User.findOne({ where: { email }, raw: true })) {
            errors.push("Email already used");
          }
          if (errors.length === 0) {
            user = await User.create({
              firstName,
              lastName,
              email,
              type: 2,
              token: Math.random().toString(36)
            });
          } else {
            res.status(400).send({ errors });
          }
        }
        if (errors.length === 0) {
          req.session.id = user.id;
          req.session.token = user.token;

          const professor = await User.findOne({
            attributes: ["firstName", "lastName", "email"],
            where: { id: activity.userId },
            raw: true
          });

          res.status(200).send({
            activity: {
              ...activity.get({ plain: true }),
              professor,
              userId: undefined
            },
            message: "Access granted"
          });
        }
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  },
  giveFeedback: async (req, res) => {
    try {
      const { emoticon, activityId } = req.body;

      const activity = await Activity.findOne({
        attributes: [
          "id",
          "description",
          "duration",
          "createdAt",
          "userId",
          "status"
        ],
        where: { id: activityId }
      });

      if (!activity) {
        res.status(400).send({ message: `Activity not exists` });
      } else {
        if (activity.status) {
          await Feedback.create({
            emoticon,
            activityId,
            userId: req.session.id
          });
          res.status(201).send({ message: `Feedback was sent` });
        } else {
          res.status(403).send({ message: `Activity finished` });
        }
      }
    } catch (e) {
      console.error(e);
      res.status(500).send({
        message: "Error"
      });
    }
  }
};

module.exports = controller;
