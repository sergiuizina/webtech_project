module.exports = function(sequelize, DataTypes) {
  return sequelize.define("activity", {
    duration: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    description: DataTypes.STRING,
    accessCode: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE(6),
      allowNull: true,
      defaultValue: sequelize.fn("NOW")
    }
  });
};
