module.exports = function(sequelize, DataTypes) {
  return sequelize.define("user", {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    type: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE(6),
      allowNull: true,
      defaultValue: sequelize.fn("NOW")
    }
  });
};
