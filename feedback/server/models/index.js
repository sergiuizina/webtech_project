const sequelize = require("../config/db");

const Feedback = sequelize.import("./feedback.js");
const Activity = sequelize.import("./activity.js");
const User = sequelize.import("./user.js");

User.hasMany(Activity, { onDelete: "Cascade" });
Activity.hasMany(Feedback, { onDelete: "Cascade" });
User.hasMany(Feedback, { onDelete: "Cascade" });

module.exports = { Feedback, Activity, User, sequelize };
