module.exports = function(sequelize, DataTypes) {
  return sequelize.define("feedback", {
    emoticon: DataTypes.INTEGER,
    createdAt: {
      type: DataTypes.DATE(6),
      allowNull: true,
      defaultValue: sequelize.fn("NOW")
    }
  });
};
